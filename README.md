INSTALL
=======

For DEV
-------

```bash
make build-dev
docker/bin/composer install
docker/bin/console migration:migrate -f
```
