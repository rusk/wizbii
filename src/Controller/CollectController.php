<?php

namespace App\Controller;

use App\Service\CollectService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use function json_decode;

class CollectController extends AbstractFOSRestController
{
    public function collect(Request $request, CollectService $collectService)
    {
        if ($request->isMethod('POST')) {
            $data = json_decode($request->getContent(), true);
        } elseif ($request->isMethod('GET')) {
            $data = [$request->query->all()];
        }

        $violations = $collectService->valid($data);
        if ($violations->count()) {
            return new JsonResponse($this->violationToArray($violations), 400);
        }
        $collectService->save($data);

        return new JsonResponse(null, 201);
    }

    private function violationToArray(ConstraintViolationListInterface $violations): array
    {
        $array = [];
        /** @var ConstraintViolationInterface $violation */
        foreach ($violations as $violation) {
            $array[$violation->getPropertyPath()] = $violation->getMessage();
        }

        return $array;
    }
}
