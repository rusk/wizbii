<?php

namespace App\Service;

use App\Entity\Collect;
use App\Repository\CollectRepository;
use FLE\JsonHydrator\Repository\RepositoryFactory;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Validation;

class CollectService
{
    /**
     * @var RepositoryFactory
     */
    private $repositoryFactory;
    /**
     * @var int
     */
    private $queueTime;

    /**
     * @param RepositoryFactory $repositoryFactory
     * @param int               $queueTime
     */
    public function __construct(RepositoryFactory $repositoryFactory, int $queueTime)
    {
        $this->repositoryFactory = $repositoryFactory;
        $this->queueTime         = $queueTime;
    }

    public function valid(array $data): ConstraintViolationListInterface
    {
        $validator = Validation::createValidator();

        $eventMandatory = function ($value, ExecutionContextInterface $context) {
            if ($context->getRoot()['t'] == 'event' && $value === null) {
                $context->buildViolation('ec is mandatory for event hit type')
                    ->atPath('ec')
                    ->addViolation();
            }
        };
        $constraint = new Assert\Collection(
            ['fields' => [
                'v'    => new Assert\EqualTo(1),
                't'    => new Assert\Choice(['pageview', 'screenview', 'event']),
                'dl'   => new Assert\Optional(),
                'dr'   => new Assert\Optional(),
                'wct'  => new Assert\Choice(['profile', 'recruiter', 'visitor', 'wizbii_employee']),
                'wui'  => new Assert\NotNull(),
                'wuui' => new Assert\NotNull(),
                'ec'   => new Assert\Callback($eventMandatory),
                'ea'   => new Assert\Callback($eventMandatory),
                'el'   => new Assert\Optional(),
                'ev'   => [new Assert\Optional(), new Assert\Type('int')],
                'tid'  => new Assert\NotNull(),
                'ds'   => new Assert\Choice(['web', 'apps', 'backend']),
                'cn'   => new Assert\Optional(),
                'cs'   => new Assert\Optional(),
                'cm'   => new Assert\Optional(),
                'ck'   => new Assert\Optional(),
                'cc'   => new Assert\Optional(),
                'sn'   => new Assert\Callback(function ($value, ExecutionContextInterface $context) {
                    if ($context->getRoot()['t'] == 'screenview' && $value === null) {
                        $context->buildViolation('sn is mandatory for event hit type')
                            ->atPath('sn')
                            ->addViolation();
                    }
                }),
                'an'   => new Assert\Callback(function ($value, ExecutionContextInterface $context) {
                    if ($context->getRoot()['t'] == 'apps' && $value === null) {
                        $context->buildViolation('an is mandatory for event hit type')
                            ->atPath('an')
                            ->addViolation();
                    }
                }),
                'av'   => new Assert\Optional(),
                'qt'   => [new Assert\Optional(), new Assert\Type('int'), new Assert\LessThanOrEqual($this->queueTime)],
            ]]
        );

        $allViolations = new ConstraintViolationList();
        foreach ($data as $datum) {
            $violations = $validator->validate($datum, $constraint);
            $allViolations->addAll($violations);
        }

        return $allViolations;
    }

    public function save($data): void
    {
        /** @var CollectRepository $collectRepository */
        $collectRepository = $this->repositoryFactory->getRepository(Collect::class);
        $collectRepository->insert($data);
    }
}
