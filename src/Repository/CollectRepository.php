<?php

namespace App\Repository;

use FLE\JsonHydrator\Repository\AbstractRepository;
use function json_encode;

class CollectRepository extends AbstractRepository
{
    public function insert($data)
    {
        $stmt = $this->prepareRequest('insert');
        $json = json_encode($data);
        $stmt->bindParam('data', $json);

        return $stmt->fetchAsArray();
    }
}
