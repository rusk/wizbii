<?php

namespace App\Entity;

class Collect
{
    protected $uuid;
    protected $created_at;
    protected $t;
    protected $dl;
    protected $dr;
    protected $wct;
    protected $wui;
    protected $wuui;
    protected $ec;
    protected $ea;
    protected $el;
    protected $ev;
    protected $tid;
    protected $ds;
    protected $cn;
    protected $cs;
    protected $cm;
    protected $ck;
    protected $cc;
    protected $sn;
    protected $an;
    protected $av;
    protected $qt;
}
