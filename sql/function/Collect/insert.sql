CREATE OR REPLACE FUNCTION collect_insert (data json) RETURNS VOID
  LANGUAGE plpgsql
  PARALLEL SAFE
AS $$
DECLARE
  _sql text;
  _current_year timestamp = date_trunc('year', clock_timestamp());
BEGIN
    _sql := 'CREATE TABLE IF NOT EXISTS collect_' || date_part('year', _current_year) || ' PARTITION OF collect FOR VALUES FROM (''' || _current_year || ''') TO (''' || _current_year + INTERVAL 'P1Y' || ''')';
    RAISE NOTICE '%', _sql;
    EXECUTE _sql;

    INSERT INTO collect (t, dl, dr, wct, wui, wuui, ec, ea, el, ev, tid, ds, cn, cs, cm, ck, cc, sn, an, av, qt)
    SELECT j.t, j.dl, j.dr, j.wct, j.wui, j.wuui, j.ec, j.ea, j.el, j.ev, j.tid, j.ds, j.cn, j.cs, j.cm, j.ck, j.cc, j.sn, j.an, j.av, j.qt
    FROM json_populate_recordset(NULL::collect, data) j
    LEFT JOIN collect l ON l.created_at > (now() - INTERVAL '1 second') AND l.wui = j.wui
    WHERE l IS NULL;
END;
$$;
