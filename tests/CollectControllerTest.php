<?php
/**
 * Copyright (c) 2019 Ekosport <contact@groupefrasteya.com>.
 *
 * This file is part of Ekosport website.
 *
 * Ekosport website can not be copied and/or distributed without the express permission of the CIO.
 */

namespace App\Tests\Controller;

use App\Controller\CollectController;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use function json_decode;
use function json_encode;

class CollectControllerTest extends WebTestCase
{
    public function provider()
    {
        return [
            ['data' => [
                't'    => 'event',
                'dl'   => 'company/wizbii',
                'dr'   => 'job/dev-backend-chez-wizbii',
                'wct'  => 'profile',
                'wui'  => 'remi-alvado',
                'wuui' => '38b728b0e0b4f594760d4b3e58797ae1',
                'ec'   => 'navigation',
                'ea'   => 'tap',
                'el'   => 'button-top',
                'ev'   => 1,
                'tid'  => 'UA-12345-Y',
                'ds'   => 'apps',
                'sn'   => 'company',
                'an'   => 'WizbiiStudentAndroid',
                'av'   => '1.2.1',
                'qt'   => 123,
                'v'    => 1,
            ]],
            ['data' => [
                't'    => 'pageview',
                'dl'   => 'http://www.wizbii.com/bar',
                'wct'  => 'profile',
                'wui'  => 'remi-alvado',
                'wuui' => '38b728b0e0b4f594760d4b3e58797ae1',
                'ec'   => 'ads',
                'el'   => 'client',
                'ev'   => 1,
                'tid'  => 'UA-12345-Y',
                'ea'   => 'Click Masthead',
                'ds'   => 'web',
                'sn'   => 'company',
                'an'   => 'WizbiiStudentAndroid',
                'cn'   => 'bpce',
                'cs'   => 'wizbii',
                'cm'   => 'web',
                'ck'   => 'erasmus',
                'cc'   => 'foobar',
                'qt'   => 123,
                'v'    => 1,
            ]],
        ];
    }

    public function providerWrong()
    {
        return [
            ['data' => [
                't'    => 'event',
                'dl'   => 'company/wizbii',
                'dr'   => 'job/dev-backend-chez-wizbii',
                'wct'  => 'wrong value',
                'wui'  => 'remi-alvado',
                'wuui' => '38b728b0e0b4f594760d4b3e58797ae1',
                'ec'   => 'navigation',
                'ev'   => '1',
                'tid'  => 'UA-12345-Y',
                'ds'   => 'apps',
                'sn'   => 'company',
                'an'   => 'WizbiiStudentAndroid',
                'av'   => '1.2.1',
                'qt'   => '123',
                'v'    => 2,
            ]],
        ];
    }

    /**
     * @dataProvider provider
     *
     * @param array $data
     *
     * @see          CollectController::collect()
     */
    public function testCollect(array $data)
    {
        self::bootKernel();
        $client = static::createClient();
        $client->setServerParameter('HTTP_Accept', 'application/json');
        $client->request('POST', '/collect', [], [], ['HTTP_Content_Type' => 'application/json'], json_encode([$data]));

        $contentRaw = $client->getResponse()->getContent();
        $this->assertEquals(201, $client->getResponse()->getStatusCode(), $contentRaw);

        $content = json_decode($contentRaw, true);
        $this->assertIsArray($content);
    }

    /**
     * @dataProvider providerWrong
     *
     * @param array $data
     *
     * @see          CollectController::collect()
     */
    public function testCollectWrong(array $data)
    {
        self::bootKernel();
        $client = static::createClient();
        $client->setServerParameter('HTTP_Accept', 'application/json');
        $client->request('POST', '/collect', [], [], ['HTTP_Content_Type' => 'application/json'], json_encode([$data]));

        $contentRaw = $client->getResponse()->getContent();
        $this->assertEquals(400, $client->getResponse()->getStatusCode(), $contentRaw);

        $content = json_decode($contentRaw, true);
        $this->assertIsArray($content);
        $this->assertGreaterThan(0, $content);
    }

    /**
     * @dataProvider provider
     *
     * @param array $data
     *
     * @see          CollectController::collect()
     */
    public function testCollectGet(array $data)
    {
        self::bootKernel();
        $client = static::createClient();
        $client->setServerParameter('HTTP_Accept', 'application/json');
        $client->request('GET', '/collect', $data);

        $contentRaw = $client->getResponse()->getContent();
        $this->assertEquals(201, $client->getResponse()->getStatusCode(), $contentRaw);

        $content = json_decode($contentRaw, true);
        $this->assertIsArray($content);
    }
}
