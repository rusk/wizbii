CREATE TABLE collect
(
    uuid       uuid      NOT NULL DEFAULT uuid_generate_v4(),
    created_at timestamp NOT NULL DEFAULT now(),
    t          text      NOT NULL check ( t = ANY ('{pageview, screenview, event}')),
    dl         TEXT      NULL,
    dr         TEXT      NULL,
    wct        TEXT      NOT NULL check ( wct = ANY ('{profile, recruiter, visitor, wizbii_employee}')),
    wui        TEXT      NOT NULL,
    wuui       TEXT      NOT NULL,
    ec         TEXT      NULL check ( t != 'event' OR ec IS NOT NULL ),
    ea         TEXT      NULL check ( t != 'event' OR ea IS NOT NULL ),
    el         TEXT      NULL,
    ev         int       NULL,
    tid        TEXT      NOT NULL,
    ds         TEXT      NULL check ( ds = ANY ('{web, apps, backend}')),
    cn         TEXT      NULL,
    cs         TEXT      NULL,
    cm         TEXT      NULL,
    ck         TEXT      NULL,
    cc         TEXT      NULL,
    sn         TEXT      NULL check ( t != 'screenview ' OR ea IS NOT NULL ),
    an         TEXT      NULL check ( t != 'apps' OR ea IS NOT NULL ),
    av         TEXT      NULL,
    qt         int       NULL,
    PRIMARY KEY (created_at, uuid)
) PARTITION BY RANGE (created_at);