include .env

help:
	@echo "Please choose one of the following target:"
	@echo make update
	@echo make build-dev
	@echo make reset-all-data
	@echo make tests

update:
	git pull
	docker-compose up --build -d
	./docker/bin/composer install --no-dev -o
	./docker/bin/console migration:migrate --force

build-dev:
	docker-compose down --remove-orphans; docker-compose -f docker-compose.dev.yml down --remove-orphans
	docker-compose up --build -d
	docker-compose -f docker-compose.dev.yml up --build -d

prompt:
	@if [ $(shell bash -c 'read -s -p "Are you sure ? (yes/no)" pwd; echo $$pwd') != 'yes' ]; then echo "\n\nABORT"; exit 1; else echo "\n"; fi;

reset-all-data: prompt
	docker-compose down --remove-orphans; docker-compose -f docker-compose.dev.yml down --remove-orphans
	sudo rm -rf ./var/postgresql
	sudo rm -rf ./var/cache/*
	sudo rm -rf ./var/tmp/nginx/*
	docker-compose build
	docker-compose -f docker-compose.dev.yml build
	docker-compose up --build -d
	docker-compose -f docker-compose.dev.yml up --build -d
	./docker/bin/console migration:migrate --force

tests: reset-all-data
	docker-compose -f docker-compose.dev.yml run web-debug /www/wizbii/vendor/bin/phpunit --configuration /www/wizbii/phpunit.xml.dist
